//
//  NaviViewController.h
//  finalproject
//
//  Created by zhaoyixuan on 3/30/14.
//  Copyright (c) 2014 zhaoyixuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NaviViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableViewCell *disChoose;

@end
