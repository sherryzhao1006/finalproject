//
//  PostBookViewController.h
//  finalproject
//
//  Created by zhaoyixuan on 3/29/14.
//  Copyright (c) 2014 zhaoyixuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostBookViewController : UIViewController<UITextInputDelegate,UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *loca;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *offetButton;
-(void) animateTextView:(UITextView *)textView up:(BOOL)up;
- (IBAction)isSelected:(id)sender;
-(void)offerselect;
@end
