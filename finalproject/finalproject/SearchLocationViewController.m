//
//  SearchLocationViewController.m
//  finalproject
//
//  Created by zhaoyixuan on 3/29/14.
//  Copyright (c) 2014 zhaoyixuan. All rights reserved.
//

#import "SearchLocationViewController.h"

@interface SearchLocationViewController ()
- (void)configureView;
@end

@implementation SearchLocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _dataSource=[[NSMutableArray alloc]init];
    _dataBase=[[NSMutableArray alloc]init];
    for (int i=0; i<50; i++) {
        [_dataSource  addObject:[NSString stringWithFormat:@"%d",i]];
        [_dataBase addObject:[NSString stringWithFormat:@"%d",i]];
    }
    UISearchBar *searchBar=[[UISearchBar alloc]init];
    searchBar.frame=CGRectMake(0, 0, self.view.bounds.size.width+20, 0);
    searchBar.delegate=self;
    searchBar.keyboardType=UIKeyboardTypeDefault;
    searchBar.showsCancelButton=YES;
    searchBar.showsSearchResultsButton=YES;
    searchBar.translucent=YES;
    searchBar.barStyle=UIBarStyleDefault;
    searchBar.placeholder=@"input";
    //searchBar.prompt=@"search";
    [searchBar sizeToFit];
    self.tableView.tableHeaderView=searchBar;
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil];
    
    //[self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_dataSource count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str=@"Cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:str forIndexPath:indexPath];
    cell.textLabel.text=[_dataSource objectAtIndex:indexPath.row];
    if (_dataSource.count==0) {
        cell.textLabel.text=@"Sorry, we didn't see and matched book";
    }
    return cell;
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [_dataSource removeAllObjects];
    NSString *data;
    for(data in _dataBase){
        if ([data hasPrefix:searchBar.text]) {
            [_dataSource addObject:data];
        }
    }
    
    UITableViewCell *cell=(UITableViewCell *)[self.view viewWithTag:1];
    if (_dataSource.count==0) {
        cell.textLabel.text=@"Sorry, we didn't see and matched book";
    }
    [self.tableView reloadData];
    [searchBar resignFirstResponder];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchBar.text=@" ";
    [searchBar resignFirstResponder];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (0==searchText.length) {
        return;
    }
    [_dataSource removeAllObjects];
    NSString *str;
    UITableViewCell *cell=(UITableViewCell *)[self.view viewWithTag:1];
    for(str in _dataBase){
        if ([str hasPrefix:searchText]) {
            [_dataSource addObject:str];
        }
        else
        {
            //cell.textLabel.text=@"Sorry, we didn't see and matched book";
        }
        
    }
    if (_dataSource.count==0) {
        cell.textLabel.text=@"Sorry, we didn't see and matched book";
    }
    [self.tableView reloadData];
}


@end
