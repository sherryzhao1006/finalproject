//
//  PostBookViewController.m
//  finalproject
//
//  Created by zhaoyixuan on 3/29/14.
//  Copyright (c) 2014 zhaoyixuan. All rights reserved.
//

#import "PostBookViewController.h"
#import "DiscoverBookViewController.h"
@interface PostBookViewController ()<UITextViewDelegate>
{
    DiscoverBookViewController *discover;

}
@end

@implementation PostBookViewController{
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.toolbarHidden=NO;
    
}
-(void)isSelected:(id)sender{
    //_offetButton.target=discover;
    //_offetButton.action=@selector(offerselect);
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)offerselect{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) animateTextView:(UITextView *)textView up:(BOOL)up{
    const int movedis=210;
    const float movedur=0.3f;
    int movement=(up ?-movedis:movedis);
    [UIView beginAnimations:@"anim" context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:movedur];
    self.view.frame=CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


-(void)textViewDidBeginEditing:(UITextView *)textView{
    NSLog(@"a");
    [self animateTextView:textView up:YES];
    //[textView resignFirstResponder];
    }
-(void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"b");
    [self animateTextView:textView up:NO];
    [textView resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
