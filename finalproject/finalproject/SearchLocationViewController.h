//
//  SearchLocationViewController.h
//  finalproject
//
//  Created by zhaoyixuan on 3/29/14.
//  Copyright (c) 2014 zhaoyixuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchLocationViewController : UITableViewController<UISearchBarDelegate,UISearchDisplayDelegate>
@property (strong, nonatomic) id detailItem;
@property (nonatomic,strong)NSMutableArray *dataSource;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic,strong)NSMutableArray *dataBase;
@property (nonatomic,strong)UISearchDisplayController *mySearchDisplayController;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end
