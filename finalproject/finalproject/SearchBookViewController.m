//
//  DetailViewController.m
//  finalproject
//
//  Created by zhaoyixuan on 3/29/14.
//  Copyright (c) 2014 zhaoyixuan. All rights reserved.
//

#import "SearchBookViewController.h"
#import "BookInfoViewController.h"
@interface SearchBookViewController ()
@property BookInfoViewController *book;
- (void)configureView;
@end

@implementation SearchBookViewController
{
}
#pragma mark - Managing the detail item
-(void)awakeFromNib{
   // _dataSource=@[@"abc",@"ab",@"cde",@"ac"];

}
- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.detailDescriptionLabel.text = [[self.detailItem valueForKey:@"timeStamp"] description];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //self.navigationController.navigationBar.hidden = YES;
    //[self.view viewWithTag:2];
    _dataSource=[[NSMutableArray alloc]init];
    _dataBase=[[NSMutableArray alloc]init];
    for (int i=0; i<50; i++) {
        [_dataSource  addObject:[NSString stringWithFormat:@"%d",i]];
        [_dataBase addObject:[NSString stringWithFormat:@"%d",i]];
    }
   UISearchBar *searchBar=[[UISearchBar alloc]init];
    searchBar.frame=CGRectMake(0, 0, self.view.bounds.size.width+20, 0);
  searchBar.delegate=self;
    searchBar.keyboardType=UIKeyboardTypeDefault;
    searchBar.showsCancelButton=YES;
    searchBar.showsSearchResultsButton=YES;
   searchBar.translucent=YES;
    searchBar.barStyle=UIBarStyleDefault;
    searchBar.placeholder=@"input";
    //searchBar.prompt=@"search";
    [searchBar sizeToFit];
    self.tableView.tableHeaderView=searchBar;
   self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
   return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_dataSource count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str=@"Cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:str forIndexPath:indexPath];
    cell.textLabel.text=[_dataSource objectAtIndex:indexPath.row];
    if (_dataSource.count==0) {
        cell.textLabel.text=@"Sorry, we didn't see and matched book";
    }
    return cell;
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [_dataSource removeAllObjects];
    NSString *data;
    for(data in _dataBase){
        if ([data hasPrefix:searchBar.text]) {
            [_dataSource addObject:data];
        }
    }
    
    [self.tableView reloadData];
    [searchBar resignFirstResponder];
        }
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchBar.text=@" ";
    [searchBar resignFirstResponder];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (0==searchText.length) {
        return;
    }
    [_dataSource removeAllObjects];
    NSString *str;
    UITableViewCell *cell=(UITableViewCell *)[self.view viewWithTag:1];
    for(str in _dataBase){
        if ([str hasPrefix:searchText]) {
            [_dataSource addObject:str];
        }
        else
        {
            //cell.textLabel.text=@"Sorry, we didn't see and matched book";
        }
        
    }
    if (_dataSource.count==0) {
cell.textLabel.text=@"Sorry, we didn't see and matched book";
    }
    [self.tableView reloadData];
}
@end
