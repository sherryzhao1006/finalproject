//
//  MasterViewController.h
//  finalproject
//
//  Created by zhaoyixuan on 3/29/14.
//  Copyright (c) 2014 zhaoyixuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "PKRevealController.h"
#import <CoreData/CoreData.h>

@interface DiscoverBookViewController : UITableViewController <NSFetchedResultsControllerDelegate,UIViewControllerAnimatedTransitioning>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong,nonatomic)PKRevealController *revealControler;
-(void)clickComposeButton;
-(void)clickSearchButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideButton;
-(void)showLeftView;
@end
