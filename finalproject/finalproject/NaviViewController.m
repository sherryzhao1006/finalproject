//
//  NaviViewController.m
//  finalproject
//
//  Created by zhaoyixuan on 3/30/14.
//  Copyright (c) 2014 zhaoyixuan. All rights reserved.
//

#import "NaviViewController.h"
#import "DiscoverBookViewController.h"
#import "MineViewController.h"
#import "BookInfoViewController.h"
@interface NaviViewController ()
@property (strong,nonatomic) DiscoverBookViewController *discoverBookViewController;
@property (strong,nonatomic) BookInfoViewController *bookInfo;
@property(strong,nonatomic) MineViewController *mine;

@end

@implementation NaviViewController{
    NSArray *_data;
}
@synthesize discoverBookViewController=_discoverBookViewController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _data=@[@"Discover",@"Mine",@"Message",@"Feedback"];
    self.view.bounds=CGRectMake(0, 0, 200, 200);
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil];

    //self.view.frame=CGRectMake(0, 0, 50, 50);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    NSLog(@"%ld",(long)indexPath.row);
}
/*-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellstr=@"Cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellstr];
    
        cell.textLabel.text=_data[indexPath.row];
    
    return cell;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        case 1:
            [self.navigationController popToViewController:self.mine animated:YES];
            break;
        default:
            break;
    }
    
}*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
